FROM python:3
WORKDIR /usr/src/app
RUN pip install psycopg2
COPY . .
CMD [ "python", "./Eurobusiness.py" ]
