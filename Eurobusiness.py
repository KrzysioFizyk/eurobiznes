from org.krzysio.cards.CityCards import CityCards
from org.krzysio.Board import Board
import psycopg2
import sys

max_moves_per_game = 100 * 1000
single_row_games = 1000
cash_diff_threshold = 100 * 1000


def get_db_cur():
    try:
        conn = psycopg2.connect(host="krisso-pg2.postgres.database.azure.com", user="postgres@krisso-pg2",
                                password="Krisso@azure1", dbname="postgres")
        return conn.cursor()
    except:
        print("Nie można połączyć się z bazą")
        raise AssertionError()


def run_single_game(init_cash1, safety_factor1):
    board = Board(init_cash1, safety_factor1)
    for i in range(max_moves_per_game):
        board.next_move()

        if len(board.players) == 1 or board.two_players_fully_developed(cash_diff_threshold):
            break

        board.currentPlayerIdx += 1
        if board.currentPlayerIdx > len(board.players) - 1:
            board.currentPlayerIdx = 0

    active_players = []
    active_players.extend(board.players)
    active_players.sort(key=lambda p1: p1.calculate_total_value(), reverse=True)

    # for p in active_players:
    #     print("{}. {}, Total value: {}".format(p.playerId, p.player_country, p.calculate_total_value()))

    return active_players[0].player_country


def write_results_to_pg(job_winners_dict):
    cur1 = get_db_cur()

    for db_row_id, winners_dict in job_winners_dict.items():
        total_games = sum(winners_dict.values())

        update_sql = "UPDATE eurobiznes " \
                     "SET total_games = total_games + {}, " \
                     "grecja = grecja + {}, " \
                     "wlochy = wlochy + {}, " \
                     "hiszpania = hiszpania + {}, " \
                     "anglia = anglia + {}, " \
                     "benelux = benelux + {}, " \
                     "szwecja = szwecja + {}, " \
                     "rfn = rfn + {}, " \
                     "austria = austria + {} " \
                     "WHERE id = {} "

        update_sql = update_sql.format(total_games, winners[CityCards.Grecja], winners[CityCards.Wlochy],
                                       winners[CityCards.Hiszpania], winners[CityCards.Anglia],
                                       winners[CityCards.Benelux], winners[CityCards.Szwecja],
                                       winners[CityCards.RFN], winners[CityCards.Austria],
                                       db_row_id)

        cur1.execute(update_sql)

    cur1.connection.commit()
    cur1.close()
    cur1.connection.close()

    print("Wyniki zapisano w bazie danych.")
    print("")
    sys.stdout.flush()


def get_jobs():
    jobs_per_turn = -1
    max_total_games = -1

    cur = get_db_cur()
    cur.execute("SELECT name, value FROM manage")
    rows = cur.fetchall()

    for row in rows:
        config_name = row[0]
        config_value = row[1]

        if config_name == "keep.going":
            if config_value == "false":
                print("Kończymy. keep.going=false lub robota skończona. Dzięki!")
                # return []
        elif config_name == "jobs_per_turn":
            jobs_per_turn = int(config_value)
        elif config_name == "max.total_games":
            max_total_games = int(config_value)

    cur.execute("SELECT id, init_cash, safety_factor "
                "FROM eurobiznes "
                "WHERE total_games < {}"
                "ORDER BY total_games, last_attempt "
                "LIMIT {}".format(max_total_games, jobs_per_turn))
    rows = cur.fetchall()

    if len(rows) == 0:
        return []

    all_row_ids = []
    for row in rows:
        all_row_ids.append(str(row[0]))

    cur.execute("UPDATE eurobiznes SET last_attempt = now() WHERE id IN ({})".format(",".join(all_row_ids)))
    cur.connection.commit()

    cur.close()
    cur.connection.close()

    return rows


# execution starts here
if __name__ == "__main__":
    print("Startujemy! :-)")
    sys.stdout.flush()

    while True:
        jobs = get_jobs()
        if len(jobs) == 0:
            break

        job_winners = dict()

        for job in jobs:
            row_id = int(job[0])
            init_cash = int(job[1])
            safety_factor = float(job[2])

            winners = {CityCards.Grecja: 0, CityCards.Wlochy: 0, CityCards.Hiszpania: 0, CityCards.Anglia: 0,
                       CityCards.Benelux: 0, CityCards.Szwecja: 0, CityCards.RFN: 0, CityCards.Austria: 0}

            print("Symulacje dla (init_cash, safety_factor) = ({}, {})".format(init_cash, safety_factor))
            sys.stdout.flush()

            for n in range(single_row_games):
                winner = run_single_game(init_cash, safety_factor)
                winners[winner] += 1

            print("Wyniki: ", winners)
            print("")
            sys.stdout.flush()
            job_winners[row_id] = winners

        write_results_to_pg(job_winners)
