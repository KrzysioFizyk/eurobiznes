import psycopg2

conn = psycopg2.connect(host="localhost", user="postgres",
                        password="postgres", dbname="postgres")
cur = conn.cursor()

cur.execute("SELECT init_cash, safety_factor, total_games, "
            "grecja, wlochy, hiszpania, anglia, benelux, szwecja, rfn, austria "
            "FROM eurobiznes ORDER BY init_cash, safety_factor")
rows = cur.fetchall()
cur.close()
conn.close()

normalized_results = open("/Users/krisso/Desktop/Eurobiznes_normalized_results.csv", "w")
normalized_results.write("init_cash,safety_factor,total_games,"
                         "grecja,wlochy,hiszpania,anglia,benelux,szwecja,rfn,austria\n")

winners = open("/Users/krisso/Desktop/Eurobiznes_winners.csv", "w")
winners.write("init_cash,safety_factor,winner\n")

for row in rows:
    init_cash = int(row[0])
    safety_factor = float(row[1])
    total_games = int(row[2])
    grecja = int(row[3])
    wlochy = int(row[4])
    hiszpania = int(row[5])
    anglia = int(row[6])
    benelux = int(row[7])
    szwecja = int(row[8])
    rfn = int(row[9])
    austria = int(row[10])

    player_wins = row[3:]
    max_wins = max(player_wins)
    country_number = 0

    if player_wins.count(max_wins) == 1:
        country_number = player_wins.index(max_wins) + 1
    else:
        print(row)

    winners.write("{},{},{}\n".format(init_cash, safety_factor, country_number))

    line = "{},{},{},{},{},{},{},{},{},{},{}\n" \
        .format(init_cash, safety_factor, total_games,
                grecja / total_games, wlochy / total_games, hiszpania / total_games, anglia / total_games,
                benelux / total_games, szwecja / total_games, rfn / total_games, austria / total_games)
    normalized_results.write(line)

normalized_results.flush()
normalized_results.close()

winners.flush()
winners.close()
