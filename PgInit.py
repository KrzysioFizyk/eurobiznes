# Just helper file to create initialing SQL

sql_file = open("/Users/krisso/Desktop/eurobiznes-init.sql", "w")

sql = "INSERT INTO eurobiznes(init_cash, safety_factor) VALUES "

for init_cash in range(1840, 4010, 20):

    for safety_factor_100 in range(0, 201, 2):
        safety_factor = safety_factor_100 / 100.0

        sql += "\n({}, {}),".format(init_cash, safety_factor)

sql = sql[0:-1] + ";"
print(sql)

sql_file.write(sql)
sql_file.write("\n\n")
sql_file.flush()
sql_file.close()
