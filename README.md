## About
Prosty program w Pythonie symulujący grę Eurobusiness.

## Cel
Celem symulacji było znalezienie odpowiedzi na pytanie 
**"Które państwo ma największe szanse na wygraną?"**

## Założenia
Parametry wejściowe symulacji:

- **Kapitał początkowy.**

- **Współczynnik ostrożności.** Jest to odwrotność "agresywności" gracza.
Im mniejsza wartość współczynnika ostrożności, tym większą agresywnością
cechuje się gracz. W szczególności wartość 0 oznacza, że gracz inwestuje
cały kapitał, jaki posiada. Wartość 1 oznacza, że gracz zachowuje minimum
tyle kapitału, aby móc opłacić aktualnie najdroższe pole na planszy.
Wartość 2 oznacza, że gracz zachowuje minimum 2-krotność opłaty aktualnie
najdroższego pola na planszy.

Każde państwo było osobnych graczem. Dla pozbycia się wszelkich zaburzeń
przyjęto następujące upraszczające założenia, które nie wpływają
na wyniki (największy wpływ na wyniki ma tak naprawdę kolejność odwiedzanych pól):
- Koleje, Wodociągi i Elektrownia są traktowane jako pola neutralne

- Wyjście z więzienia jest darmowe (sama strategia wychodzenia z więzienia
  zasługuje na osobną pracę), przy czym karty "Idziesz do więzienia",
  pole 31. oraz 3-krotny dublet wysyłają gracza do więzienia.

- Nie ma limitu na ilość domków/hoteli na planszy. W rzeczywistości
  niekiedy specjalnie nie kupuje się hotelu, aby inni gracze nie mieli
  możliwości inwestowania.

- Kiedy gracz przegrywa, cały majątek jest sprzedawany bezpośrednio do banku,
  bez możliwości przejęcia przez innych graczy.

## Obliczenia
Zdefiniowano ponad **11000** warunków początkowych. Dla każdego warunku
początkowego rozegrano co najmniej 8000 gier. W sumie wszystkich symulacji
było nieco **ponad 127 milionów.**

Warunki brzegowe zebrano w tabeli na Postgresie i każda gra 
(a właściwie paczka 1000 gier) aktualizowała ilość zwycięstw 
poszczególnych państw.

Symulacje wykonywano równolegle na wielu procesorach. W szczytowym momencie
na platformie Azure pracowało 7 maszyn (3 maszyny z 72 rdzeniami + 4 maszyny z 32 rdzeniami),
co daje **344 równoległych procesów**.

Ponieważ wyniki były docelowo normalizowane, nie było konieczności 
wymuszenia wstrzymywania obliczeń, przez co dla niektórych warunków
początkowych wykonano o wiele więcej symulacji, niż 8000. Dzięki temu
uniknięto konieczności wykonywania `select for update` na bazie danych
lub innego mechanizmu rezerowania zadań.

### Warunki końcowe
Symulacja ulega zakończeniu przy spełnieniu przynajmniej jednego
z poniższych warunków, przy czym wygrywa gracz z największym dobytkiem:
- Pozostał tylko jeden gracz

- Wykonano `100 000` rzutów kostką

- Pozostało tylko dwóch graczy i każdy jest maksymalnie rozwinięty
  i różnica posiadanej gotówki przekroczyła `$100k`

### Docker image
Dla ułatwienia zarządzania uruchamianiem zbudowano obraz Dockerowy,
który uruchamiano na poszczególnych maszynach skryptem 
[doc/run-containers.sh](doc/run-containers.sh)

## Opracowanie wyników
Wyniki opracowano przy pomocy [https://plot.ly](https://plot.ly)

#### Analiza szans wygrania przez poszczególne państwa

![Włochy](wyniki/Wlochy.jpeg)

![Hiszpania](wyniki/Hiszpania.jpeg)

![Anglia](wyniki/Anglia.jpeg)

![Benelux](wyniki/Benelux.jpeg)

![Szwecja](wyniki/Szwecja.jpeg)

![RFN](wyniki/RFN.jpeg)

![Austria](wyniki/Austria.jpeg)

#### Analiza globalna
![Global-results](wyniki/Eurobiznes-wyniki.jpeg)

[Wersja online](https://goo.gl/MrxXT8)

## Wnioski
Brakująca Grecja w wynikach nie jest przypadkiem - to państwo
w ogóle się nie liczy.

Tak naprawdę znaczenie mają 3 państwa: **Anglia, Szwecja i RFN**.
Cała reszta to "margines".

Jeśli mamy do czynienia z graczami agresywnymi, to dla kapitału początkowego
`<3060`, wygrywa Anglia, a dla kapitału początkowego `>3060` wygrywa Szwecja.
Jeśli mamy do czynienia z graczami ostrożnymi, to wygrywa RFN, niezależnie 
od kapitału początkowego.

Być może nie jest przypadkiem, że wg reguł gry kapitał początkowy
wynosi `$3000`.