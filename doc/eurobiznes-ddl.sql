CREATE SEQUENCE public.eurobiznes_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;


-- Table: public.eurobiznes

-- DROP TABLE public.eurobiznes;

CREATE TABLE public.eurobiznes
(
    id integer NOT NULL DEFAULT nextval('eurobiznes_id_seq'::regclass),
    init_cash integer NOT NULL,
    safety_factor numeric(20,5) NOT NULL,
    total_games integer NOT NULL DEFAULT 0,
    grecja integer NOT NULL DEFAULT 0,
    wlochy integer NOT NULL DEFAULT 0,
    hiszpania integer NOT NULL DEFAULT 0,
    anglia integer NOT NULL DEFAULT 0,
    benelux integer NOT NULL DEFAULT 0,
    szwecja integer NOT NULL DEFAULT 0,
    rfn integer NOT NULL DEFAULT 0,
    austria integer NOT NULL DEFAULT 0,
    last_attempt timestamp with time zone NOT NULL DEFAULT now(),
    CONSTRAINT eurobiznes_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.eurobiznes
    OWNER to postgres;

-- Index: idx_last_attempt

-- DROP INDEX public.idx_last_attempt;

CREATE INDEX idx_last_attempt
    ON public.eurobiznes USING btree
    (last_attempt)
    TABLESPACE pg_default;

-- Index: idx_total_games

-- DROP INDEX public.idx_total_games;

CREATE INDEX idx_total_games
    ON public.eurobiznes USING btree
    (total_games)
    TABLESPACE pg_default;



-- Table: public.manage

-- DROP TABLE public.manage;

CREATE TABLE public.manage
(
    name character varying COLLATE pg_catalog."default",
    value character varying COLLATE pg_catalog."default"
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.manage
    OWNER to postgres;


INSERT INTO public.manage(name, value)
VALUES
    ('keep.going', 'true'),
    ('max.total_games', '15000'),
    ('jobs_per_turn', '20');
    