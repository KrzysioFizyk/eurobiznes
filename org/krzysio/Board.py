import random
from org.krzysio.Player import Player
from org.krzysio.cards.BlueRedCards import BlueCards
from org.krzysio.cards.BlueRedCards import RedCards
from org.krzysio.cards.CityCards import CityCards


class Board:
    def __init__(self, init_cash, safety_factor):
        self.blueCards = BlueCards()
        self.redCards = RedCards()
        self.safety_factor = safety_factor

        # Grecja
        player1 = Player(0, init_cash, CityCards.Grecja)

        # Włochy
        player2 = Player(1, init_cash, CityCards.Wlochy)

        # Hiszpania
        player3 = Player(2, init_cash, CityCards.Hiszpania)

        # Anglia
        player4 = Player(3, init_cash, CityCards.Anglia)

        # Benelux
        player5 = Player(4, init_cash, CityCards.Benelux)

        # Szwecja
        player6 = Player(5, init_cash, CityCards.Szwecja)

        # RFN
        player7 = Player(6, init_cash, CityCards.RFN)

        # Austria
        player8 = Player(7, init_cash, CityCards.Austria)

        self.players = [player1, player2, player3, player4, player5, player6, player7, player8]

        self.currentPlayerIdx = random.randint(0, 7)
        self.currentRound = 1

        self.cityFields = dict()

        for city in CityCards.All_Cities_list:
            self.cityFields[city.fieldNum] = city

        # other fields are considered neutral
        self.blueCardFields = {3, 18, 34}
        self.redCardFields = {8, 23, 37}

    def get_player_by_id(self, player_id):
        for player in self.players:
            if player.playerId == player_id:
                return player

        raise AssertionError()

    def get_current_max_fee(self):
        max_fee = 400  # parking
        for city in CityCards.All_Cities_list:
            if city.owner_id is not None:
                max_fee = max(max_fee, city.get_city_fee(True))

        return max_fee

    def next_move(self):
        current_player = self.players[self.currentPlayerIdx]
        current_player.before_dice_roll(self)

        dice1 = random.randint(1, 6)
        dice2 = random.randint(1, 6)

        player_survived = self._single_move(current_player, dice1 + dice2)

        if player_survived and dice1 == dice2:
            dice1 = random.randint(1, 6)
            dice2 = random.randint(1, 6)

            player_survived = self._single_move(current_player, dice1 + dice2)

            if player_survived and dice1 == dice2:
                dice1 = random.randint(1, 6)
                dice2 = random.randint(1, 6)

                if player_survived and dice1 == dice2:
                    current_player.currentPosition = 11
                else:
                    player_survived = self._single_move(current_player, dice1 + dice2)

        if not player_survived:
            if current_player.cash > 0 or len(current_player.citiesOwned) > 0:
                raise AssertionError()

            del self.players[self.currentPlayerIdx]

    def _single_move(self, current_player, move_by_positions):
        current_position = current_player.move_by(move_by_positions)
        player_survived = True

        if current_position in self.cityFields:
            player_survived = current_player.city_entered(self.cityFields[current_position], self)
        elif current_position in self.blueCardFields:
            blue_card = self.blueCards.pop()
            player_survived = blue_card.card_chosen(self, current_player, current_position)
        elif current_position in self.redCardFields:
            red_card = self.redCards.pop()
            player_survived = red_card.card_chosen(self, current_player, current_position)
        elif current_position == 31:  # idziesz do wiezienia
            current_player.currentPosition = 11
        elif current_position == 5:  # parking: -400
            player_survived = current_player.subtract_cash(400) == 400
        elif current_position == 39:  # podatek od wzbogacenia: -200
            player_survived = current_player.subtract_cash(200) == 200

        return player_survived

    def two_players_fully_developed(self, cash_diff_threshold):
        return len(self.players) == 2 \
               and self.players[0].cash > cash_diff_threshold \
               and self.players[1].cash > cash_diff_threshold \
               and abs(self.players[0].cash - self.players[1].cash) > cash_diff_threshold \
               and self.players[0].all_cities_with_hotel() \
               and self.players[1].all_cities_with_hotel()
