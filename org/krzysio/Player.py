from org.krzysio.cards.CityCards import CityCards


class Player:
    def __init__(self, player_id, init_cash, player_country):
        self.playerId = player_id
        self.player_country = player_country
        self.cash = init_cash
        self.currentPosition = 1
        self.citiesOwned = dict()

        for city in CityCards.All_Cities_list:
            if city.country == player_country:
                self._buy_city(city)

        if self.cash < 0:
            raise AssertionError()

    def _buy_city(self, city):
        city.owner_id = self.playerId
        self.citiesOwned[city.fieldNum] = city
        self.cash -= city.price

    def get_city_fee(self, city):
        if city.fieldNum not in self.citiesOwned:
            raise AssertionError()

        return city.get_city_fee(self._owns_entire_country(city))

    def _owns_entire_country(self, city):
        """
        :return: True if current Player owns all cities from the same country as given 'city'
        """
        same_country_cities = 0
        for c in self.citiesOwned.values():
            if c.country == city.country:
                same_country_cities += 1

        if same_country_cities == 3:
            return True

        return same_country_cities == 2 and (city.country == CityCards.Grecja or city.country == CityCards.Austria)

    def before_dice_roll(self, board):
        if len(self.citiesOwned) == 0:
            return

        max_board_fee = board.get_current_max_fee()

        min_industry_level = self._find_min_industry_level()
        sorted_cities = []
        sorted_cities.extend(self.citiesOwned.values())
        sorted_cities.sort(key=lambda c: c.fieldNum, reverse=True)

        for city in sorted_cities:
            level = city.industry_level
            if level == min_industry_level and level < 5 and self.cash >= city.housePrice:
                if self.cash >= board.safety_factor * max_board_fee:
                    self.cash -= city.housePrice
                    city.industry_level += 1

    def _find_min_industry_level(self):
        min_industry_level = 5
        for city in self.citiesOwned.values():
            if city.industry_level < min_industry_level:
                min_industry_level = city.industry_level

        return min_industry_level

    def move_by(self, positions):
        self.currentPosition += positions
        if self.currentPosition > 40:
            self.currentPosition -= 40
            self.cash += 400

        return self.currentPosition

    def city_entered(self, city, board):
        """
        :return: True if current Player is still in game; False if went default
        """
        if city.owner_id == self.playerId:
            return True

        if city.owner_id is None:
            if city.country == self.player_country and self.cash >= city.price:
                self._buy_city(city)

            return True

        beneficiary = board.get_player_by_id(city.owner_id)
        fee = beneficiary.get_city_fee(city)
        paid_amount = self.subtract_cash(fee)
        beneficiary.cash += paid_amount

        if paid_amount < fee:
            return False  # bankrupt

        return True

    def subtract_cash(self, amount):
        """
        :return: amount if current Player had enough money; less than amount - which means Player lost game
        """
        while self.cash < amount:
            sold_amount = self._sell()
            if sold_amount > 0:
                self.cash += sold_amount
            else:
                break

        if self.cash >= amount:
            self.cash -= amount
            return amount
        else:
            all_money = self.cash
            self.cash = 0
            return all_money

    def _sell(self):
        """
        Sells one item to the Bank.
        :return: Amount 'earned' from selling city/house
        """
        if len(self.citiesOwned) == 0:
            return 0

        field_nums = sorted(self.citiesOwned.keys())

        for field_num in field_nums:
            city = self.citiesOwned[field_num]
            if city.industry_level > 0:
                city.industry_level -= 1
                return city.housePrice / 2

        # all houses sold, we must sell City
        field_num = field_nums[0]
        city = self.citiesOwned[field_num]
        city.owner_id = None
        del self.citiesOwned[field_num]
        return city.price

    def calculate_total_value(self):
        value = self.cash
        for city in self.citiesOwned.values():
            value += city.industry_level * city.housePrice
            value += city.price

        return value

    def all_cities_with_hotel(self):
        for city in self.citiesOwned.values():
            if city.industry_level < 5:
                return False

        return True

    def __str__(self) -> str:
        return "[{}. {}. {}]".format(self.playerId, self.player_country, self.calculate_total_value())
