class ColorCard:
    def __str__(self) -> str:
        return "[{}: {}. {}]".format(self.color, self.num, self.text)
