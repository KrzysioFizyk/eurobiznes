from org.krzysio.cards.ColorCard import ColorCard


class RedCard12(ColorCard):
    def __init__(self):
        self.num = 12
        self.color = "red"
        self.text = "Wracasz na START"

    def card_chosen(self, board, current_player, current_position):
        current_player.currentPosition = 1
        return True
