from org.krzysio.cards.ColorCard import ColorCard


class RedCard08(ColorCard):
    def __init__(self):
        self.num = 8
        self.color = "red"
        self.text = "Płacisz opłatę za szkołę 300$."

    def card_chosen(self, board, current_player, current_position):
        return current_player.subtract_cash(300) == 300
