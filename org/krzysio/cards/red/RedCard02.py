from org.krzysio.cards.ColorCard import ColorCard


class RedCard02(ColorCard):
    def __init__(self):
        self.num = 2
        self.color = "red"
        self.text = "Bank wypłaca Ci procenty w wysokości 100$"

    def card_chosen(self, board, current_player, current_position):
        current_player.cash += 100
        return True
