from org.krzysio.cards.ColorCard import ColorCard


class RedCard16(ColorCard):
    def __init__(self):
        self.num = 16
        self.color = "red"
        self.text = "Mandat za szybką jazdę. Płacisz 30$"

    def card_chosen(self, board, current_player, current_position):
        return current_player.subtract_cash(30) == 30
