from org.krzysio.cards.ColorCard import ColorCard


class RedCard14(ColorCard):
    def __init__(self):
        self.num = 14
        self.color = "red"
        self.text = "Cofasz się o 3 pola"

    def card_chosen(self, board, current_player, current_position):
        current_player.currentPosition -= 3
        return True
