from org.krzysio.cards.ColorCard import ColorCard


class RedCard01(ColorCard):
    def __init__(self):
        self.num = 1
        self.color = "red"
        self.text = "Wracasz do Madrytu"

    def card_chosen(self, board, current_player, current_position):
        current_player.currentPosition = 15
        return True
