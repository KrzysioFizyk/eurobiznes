from org.krzysio.cards.ColorCard import ColorCard


class RedCard07(ColorCard):
    def __init__(self):
        self.num = 7
        self.color = "red"
        self.text = "Idziesz do NEAPOLU. Jeżeli przechodzisz przez START otrzymasz 400$"

    def card_chosen(self, board, current_player, current_position):
        current_player.currentPosition = 7
        current_player.cash += 400
        return True
