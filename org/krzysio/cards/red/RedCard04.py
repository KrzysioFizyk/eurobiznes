from org.krzysio.cards.ColorCard import ColorCard


class RedCard04(ColorCard):
    def __init__(self):
        self.num = 4
        self.color = "red"
        self.text = "Bank wpłaca ci należne odsetki w wysokości 300$."

    def card_chosen(self, board, current_player, current_position):
        current_player.cash += 300
        return True
