from org.krzysio.cards.ColorCard import ColorCard


class RedCard03(ColorCard):
    def __init__(self):
        self.num = 3
        self.color = "red"
        self.text = "Idziesz do KOLEI WSCHODNICH. Jeżeli przechodzisz przez START otrzymasz 400$"

    def card_chosen(self, board, current_player, current_position):
        if current_player.currentPosition == 37:
            current_player.cash += 400

        current_player.currentPosition = 36
        return True
