from org.krzysio.cards.ColorCard import ColorCard


class RedCard15(ColorCard):
    def __init__(self):
        self.num = 15
        self.color = "red"
        self.text = "Zobowiązany jesteś zmodernizować swoje miasto. Płacisz do banku za każdy dom 80$ za każdy hotel 230$."

    def card_chosen(self, board, current_player, current_position):
        amount = 0
        for city in current_player.citiesOwned.values():
            if city.industry_level == 5:
                amount += 230
            elif city.industry_level > 0:
                amount += city.industry_level * 80

        if amount == 0:
            return True

        return current_player.subtract_cash(amount) == amount
