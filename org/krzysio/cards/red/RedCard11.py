from org.krzysio.cards.ColorCard import ColorCard


class RedCard11(ColorCard):
    def __init__(self):
        self.num = 11
        self.color = "red"
        self.text = "Rozwiązałeś dobrze krzyżówkę. Jako I nagrodę otrzymujesz 200$"

    def card_chosen(self, board, current_player, current_position):
        current_player.cash += 200
        return True
