from org.krzysio.cards.ColorCard import ColorCard


class RedCard05(ColorCard):
    def __init__(self):
        self.num = 5
        self.color = "red"
        self.text = "Wracasz do BRUKSELI. Jeżeli przechodzisz przez START otrzymujesz 400$."

    def card_chosen(self, board, current_player, current_position):
        if current_player.currentPosition != 37:
            current_player.cash += 400

        current_player.currentPosition = 24
        return True
