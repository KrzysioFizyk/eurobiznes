from org.krzysio.cards.ColorCard import ColorCard


class RedCard06(ColorCard):
    def __init__(self):
        self.num = 6
        self.color = "red"
        self.text = "Piłeś w czasie pracy, płacisz karę 40$"

    def card_chosen(self, board, current_player, current_position):
        return current_player.subtract_cash(40) == 40
