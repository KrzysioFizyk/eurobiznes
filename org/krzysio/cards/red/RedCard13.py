from org.krzysio.cards.ColorCard import ColorCard


class RedCard13(ColorCard):
    def __init__(self):
        self.num = 13
        self.color = "red"
        self.text = "Remontujesz swoje domy. Płacisz do banku za każdy dom 50$ za każdy hotel 200$"

    def card_chosen(self, board, current_player, current_position):
        amount = 0
        for city in current_player.citiesOwned.values():
            if city.industry_level == 5:
                amount += 200
            elif city.industry_level > 0:
                amount += city.industry_level * 50

        if amount == 0:
            return True

        return current_player.subtract_cash(amount) == amount
