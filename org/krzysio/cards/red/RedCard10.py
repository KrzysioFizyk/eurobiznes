from org.krzysio.cards.ColorCard import ColorCard


class RedCard10(ColorCard):
    def __init__(self):
        self.num = 10
        self.color = "red"
        self.text = "Idziesz do WIĘZIENIA. Nie przechodzisz przez START. NIe otrzymujesz premii 400$."

    def card_chosen(self, board, current_player, current_position):
        current_player.currentPosition = 11
        return True
