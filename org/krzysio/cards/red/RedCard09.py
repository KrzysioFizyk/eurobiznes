from org.krzysio.cards.ColorCard import ColorCard


class RedCard09(ColorCard):
    def __init__(self):
        self.num = 9
        self.color = "red"
        self.text = "Wychodzisz wolny z WIĘZIENIA. Kartę nałeży zachować do wykorzystania lub sprzedania."

    def card_chosen(self, board, current_player, current_position):
        return True
