from random import shuffle

from org.krzysio.cards.blue.BlueCard01 import BlueCard01
from org.krzysio.cards.blue.BlueCard02 import BlueCard02
from org.krzysio.cards.blue.BlueCard03 import BlueCard03
from org.krzysio.cards.blue.BlueCard04 import BlueCard04
from org.krzysio.cards.blue.BlueCard05 import BlueCard05
from org.krzysio.cards.blue.BlueCard06 import BlueCard06
from org.krzysio.cards.blue.BlueCard07 import BlueCard07
from org.krzysio.cards.blue.BlueCard08 import BlueCard08
from org.krzysio.cards.blue.BlueCard09 import BlueCard09
from org.krzysio.cards.blue.BlueCard10 import BlueCard10
from org.krzysio.cards.blue.BlueCard11 import BlueCard11
from org.krzysio.cards.blue.BlueCard12 import BlueCard12
from org.krzysio.cards.blue.BlueCard13 import BlueCard13
from org.krzysio.cards.blue.BlueCard14 import BlueCard14
from org.krzysio.cards.blue.BlueCard15 import BlueCard15
from org.krzysio.cards.blue.BlueCard16 import BlueCard16

from org.krzysio.cards.red.RedCard01 import RedCard01
from org.krzysio.cards.red.RedCard02 import RedCard02
from org.krzysio.cards.red.RedCard03 import RedCard03
from org.krzysio.cards.red.RedCard04 import RedCard04
from org.krzysio.cards.red.RedCard05 import RedCard05
from org.krzysio.cards.red.RedCard06 import RedCard06
from org.krzysio.cards.red.RedCard07 import RedCard07
from org.krzysio.cards.red.RedCard08 import RedCard08
from org.krzysio.cards.red.RedCard09 import RedCard09
from org.krzysio.cards.red.RedCard10 import RedCard10
from org.krzysio.cards.red.RedCard11 import RedCard11
from org.krzysio.cards.red.RedCard12 import RedCard12
from org.krzysio.cards.red.RedCard13 import RedCard13
from org.krzysio.cards.red.RedCard14 import RedCard14
from org.krzysio.cards.red.RedCard15 import RedCard15
from org.krzysio.cards.red.RedCard16 import RedCard16


class BlueCards:
    def __init__(self):
        self.cards = [
            BlueCard01(),
            BlueCard02(),
            BlueCard03(),
            BlueCard04(),
            BlueCard05(),
            BlueCard06(),
            BlueCard07(),
            BlueCard08(),
            BlueCard09(),
            BlueCard10(),
            BlueCard11(),
            BlueCard12(),
            BlueCard13(),
            BlueCard14(),
            BlueCard15(),
            BlueCard16()
        ]

        shuffle(self.cards)

    def pop(self):
        card = self.cards.pop(0)
        self.cards.append(card)
        return card

    def __str__(self) -> str:
        return '\n'.join(str(c) for c in self.cards)


class RedCards:
    def __init__(self):
        self.cards = [
            RedCard01(),
            RedCard02(),
            RedCard03(),
            RedCard04(),
            RedCard05(),
            RedCard06(),
            RedCard07(),
            RedCard08(),
            RedCard09(),
            RedCard10(),
            RedCard11(),
            RedCard12(),
            RedCard13(),
            RedCard14(),
            RedCard15(),
            RedCard16()
        ]

        shuffle(self.cards)

    def pop(self):
        card = self.cards.pop(0)
        self.cards.append(card)
        return card

    def __str__(self) -> str:
        return '\n'.join(str(c) for c in self.cards)
