from org.krzysio.cards.ColorCard import ColorCard


class BlueCard05(ColorCard):
    def __init__(self):
        self.num = 5
        self.color = "blue"
        self.text = "Otrzymujesz w spadku 200$"

    def card_chosen(self, board, current_player, current_position):
        current_player.cash += 200
        return True
