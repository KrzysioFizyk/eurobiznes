from org.krzysio.cards.ColorCard import ColorCard


class BlueCard14(ColorCard):
    def __init__(self):
        self.num = 14
        self.color = "blue"
        self.text = "Wychodzisz wolny z więzienia kartę należy zachować do wykorzystania lub sprzedania."

    def card_chosen(self, board, current_player, current_position):
        return True
