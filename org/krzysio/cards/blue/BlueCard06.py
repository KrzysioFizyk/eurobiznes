from org.krzysio.cards.ColorCard import ColorCard


class BlueCard06(ColorCard):
    def __init__(self):
        self.num = 6
        self.color = "blue"
        self.text = "Płacisz na budowę szpitala 400$"

    def card_chosen(self, board, current_player, current_position):
        return current_player.subtract_cash(400) == 400