from org.krzysio.cards.ColorCard import ColorCard


class BlueCard08(ColorCard):
    def __init__(self):
        self.num = 8
        self.color = "blue"
        self.text = "Płacisz koszty leczenia w wysokości 20$"

    def card_chosen(self, board, current_player, current_position):
        return current_player.subtract_cash(20) == 20
