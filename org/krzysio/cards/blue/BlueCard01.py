from org.krzysio.cards.ColorCard import ColorCard


class BlueCard01(ColorCard):
    def __init__(self):
        self.num = 1
        self.color = "blue"
        self.text = "Wracasz na START"

    def card_chosen(self, board, current_player, current_position):
        current_player.currentPosition = 1
        return True
