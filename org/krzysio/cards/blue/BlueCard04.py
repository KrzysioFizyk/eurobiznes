from org.krzysio.cards.ColorCard import ColorCard


class BlueCard04(ColorCard):
    def __init__(self):
        self.num = 4
        self.color = "blue"
        self.text = "Masz urodziny otrzymujesz od każdego gracza po 20$"

    def card_chosen(self, board, current_player, current_position):
        return True
