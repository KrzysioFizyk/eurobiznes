from org.krzysio.cards.ColorCard import ColorCard


class BlueCard16(ColorCard):
    def __init__(self):
        self.num = 16
        self.color = "blue"
        self.text = "Płacisz składkę ubezpieczenową w wysokości 20$"

    def card_chosen(self, board, current_player, current_position):
        return current_player.subtract_cash(20) == 20
