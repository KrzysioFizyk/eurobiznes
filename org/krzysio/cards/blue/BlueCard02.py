from org.krzysio.cards.ColorCard import ColorCard


class BlueCard02(ColorCard):
    def __init__(self):
        self.num = 2
        self.color = "blue"
        self.text = "Bank pomylił sie na Twoją korzysść. Otrzymujesz 400 $"

    def card_chosen(self, board, current_player, current_position):
        current_player.cash += 400
        return True
