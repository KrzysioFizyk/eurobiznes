from org.krzysio.cards.ColorCard import ColorCard


class BlueCard12(ColorCard):
    def __init__(self):
        self.num = 12
        self.color = "blue"
        self.text = "Płacisz za kartę 20$ lub ciągniesz szansę z drugiego zestawu."

    def card_chosen(self, board, current_player, current_position):
        return current_player.subtract_cash(20) == 20
