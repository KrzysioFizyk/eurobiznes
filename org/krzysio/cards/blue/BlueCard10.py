from org.krzysio.cards.ColorCard import ColorCard


class BlueCard10(ColorCard):
    def __init__(self):
        self.num = 10
        self.color = "blue"
        self.text = "Zająłeś II miejsce w konkursie piękności otrzymujesz z banku 200$."

    def card_chosen(self, board, current_player, current_position):
        current_player.cash += 200
        return True
