from org.krzysio.cards.ColorCard import ColorCard


class BlueCard13(ColorCard):
    def __init__(self):
        self.num = 13
        self.color = "blue"
        self.text = "Otrzymujesz roczną rentę w wysokości 200$."

    def card_chosen(self, board, current_player, current_position):
        current_player.cash += 200
        return True
