from org.krzysio.cards.ColorCard import ColorCard


class BlueCard07(ColorCard):
    def __init__(self):
        self.num = 7
        self.color = "blue"
        self.text = "Bank wypłaci ci należne 7% odsetek od kapitałów - otrzymujesz 50$."

    def card_chosen(self, board, current_player, current_position):
        current_player.cash += 50
        return True
