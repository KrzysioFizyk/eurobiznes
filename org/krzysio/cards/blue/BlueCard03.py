from org.krzysio.cards.ColorCard import ColorCard


class BlueCard03(ColorCard):
    def __init__(self):
        self.num = 3
        self.color = "blue"
        self.text = "Idziesz do więzienia. Nie przechodzisz przez START. Nie otrzymujesz 400$."

    def card_chosen(self, board, current_player, current_position):
        current_player.currentPosition = 11
        return True
