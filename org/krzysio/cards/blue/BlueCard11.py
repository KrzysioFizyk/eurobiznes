from org.krzysio.cards.ColorCard import ColorCard


class BlueCard11(ColorCard):
    def __init__(self):
        self.num = 11
        self.color = "blue"
        self.text = "Wracasz do WIEDNIA."

    def card_chosen(self, board, current_player, current_position):
        current_player.currentPosition = 40
        return True
