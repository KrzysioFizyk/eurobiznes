from org.krzysio.cards.ColorCard import ColorCard


class BlueCard15(ColorCard):
    def __init__(self):
        self.num = 15
        self.color = "blue"
        self.text = "Z magazynu, w którym kupujesz otrzymujesz rabat w wysokości 20$."

    def card_chosen(self, board, current_player, current_position):
        current_player.cash += 20
        return True
