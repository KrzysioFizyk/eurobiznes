from org.krzysio.cards.ColorCard import ColorCard


class BlueCard09(ColorCard):
    def __init__(self):
        self.num = 9
        self.color = "blue"
        self.text = "Otrzymujesz zwrot nadpłaconego podatku dochodowego 40$."

    def card_chosen(self, board, current_player, current_position):
        current_player.cash += 40
        return True
