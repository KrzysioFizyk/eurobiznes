class CityCard:
    def __init__(self, name, country, field_num, price, house_price,
                 fee_no_buildings, fee_1_house, fee_2_houses, fee_3_houses, fee_4_houses, fee_hotel):
        self.owner_id = None

        self.name = name
        self.country = country
        self.fieldNum = field_num
        self.price = price
        self.housePrice = house_price

        self.fee_noBuildings = fee_no_buildings
        self.fee_1House = fee_1_house
        self.fee_2Houses = fee_2_houses
        self.fee_3Houses = fee_3_houses
        self.fee_4Houses = fee_4_houses
        self.fee_Hotel = fee_hotel

        # 0 - no houses, 1 - 1 house, ... 4 - 4 houses, 5 - hotel
        self.industry_level = 0

    def get_city_fee(self, ows_entire_country):
        if self.industry_level == 0:
            if ows_entire_country:
                return 2 * self.fee_noBuildings
            else:
                return self.fee_noBuildings

        if self.industry_level == 1:
            return self.fee_1House

        if self.industry_level == 2:
            return self.fee_2Houses

        if self.industry_level == 3:
            return self.fee_3Houses

        if self.industry_level == 4:
            return self.fee_4Houses

        if self.industry_level == 5:
            return self.fee_Hotel

    def __str__(self) -> str:
        return "{}. level: {}".format(self.name, self.industry_level)


class CityCards:
    Grecja = "Grecja"
    Wlochy = "Włochy"
    Hiszpania = "Hiszpania"
    Anglia = "Anglia"
    Benelux = "Benelux"
    Szwecja = "Szwecja"
    RFN = "RFN"
    Austria = "Austria"

    # Grecja
    Saloniki = CityCard("Saloniki", Grecja, 2, 120, 100,
                         10, 40, 120, 360, 640, 900)

    Ateny = CityCard("Ateny", Grecja, 4, 120, 100,
                    10, 40, 120, 360, 640, 900)

    # Włochy
    Neapol = CityCard("Neapol", Wlochy, 7, 200, 100,
                      15, 60, 180, 540, 800, 1100)

    Mediolan = CityCard("Mediolan", Wlochy, 9, 200, 100,
                        15, 60, 180, 540, 800, 1100)

    Rzym = CityCard("Rzym", Wlochy, 10, 240, 100,
                    20, 80, 200, 600, 900, 1200)

    # Hiszpania
    Barcelona = CityCard("Barcelona", Hiszpania, 12, 280, 200,
                         20, 100, 300, 900, 1250, 1500)

    Sewilla = CityCard("Sewilla", Hiszpania, 14, 280, 200,
                       20, 100, 300, 900, 1250, 1500)

    Madryt = CityCard("Madryt", Hiszpania, 15, 320, 200,
                      25, 120, 360, 1000, 1400, 1800)

    # Anglia
    Liverpool = CityCard("Liverpool", Anglia, 17, 360, 200,
                         30, 140, 400, 1100, 1500, 1900)

    Glasgow = CityCard("Glasgow", Anglia, 19, 360, 200,
                       30, 140, 400, 1100, 1500, 1900)

    Londyn = CityCard("Londyn", Anglia, 20, 400, 200,
                      35, 160, 440, 1200, 1600, 2000)

    # Benelux
    Rotterdam = CityCard("Rotterdam", Benelux, 22, 440, 300,
                         35, 180, 500, 1400, 1750, 2100)

    Bruksela = CityCard("Bruksela", Benelux, 24, 440, 300,
                        35, 180, 500, 1400, 1750, 2100)

    Amsterdam = CityCard("Amsterdam", Benelux, 25, 480, 300,
                         40, 200, 600, 1500, 1850, 2200)

    # Szwecja
    Malmo = CityCard("Malmo", Szwecja, 27, 520, 300,
                     45, 220, 660, 1600, 1950, 2300)

    Goteborg = CityCard("Goteborg", Szwecja, 28, 520, 300,
                        45, 220, 660, 1600, 1950, 2300)

    Sztokholm = CityCard("Sztokholm", Szwecja, 30, 560, 300,
                         50, 240, 720, 1700, 2050, 2400)

    # RFN
    Frankfurt = CityCard("Frankfurt", RFN, 32, 600, 400,
                         55, 260, 780, 1900, 2200, 2550)

    Kolonia = CityCard("Kolonia", RFN, 33, 600, 400,
                       55, 260, 780, 1900, 2200, 2550)

    Bonn = CityCard("Bonn", RFN, 35, 640, 400,
                    60, 300, 900, 2000, 2400, 2800)

    # Austria
    Insbruck = CityCard("Insbruck", Austria, 38, 700, 400,
                        70, 350, 1000, 2200, 2600, 3000)

    Wieden = CityCard("Wiedeń", Austria, 40, 800, 400,
                      100, 400, 1200, 2800, 3400, 4000)

    All_Cities_list = [Saloniki, Ateny, Neapol, Mediolan, Rzym,
                  Barcelona, Sewilla, Madryt, Liverpool, Glasgow, Londyn,
                  Rotterdam, Bruksela, Amsterdam, Malmo, Goteborg, Sztokholm,
                  Frankfurt, Kolonia, Bonn, Insbruck, Wieden]